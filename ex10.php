<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<style>
    div {
        width:150px;
        height:150px;
        border: 1px solid black;
        float:left;
    }
</style>

<?php
/*
 Afficher 50 div de couleures aléatoires
 (utiliser rgb: https://www.w3schools.com/cssref/func_rgb.asp)
*/

?>

<!-- écrire le code après ce commentaire -->
<?php

    for ($d=0; $d<=50;$d++) {
        $a = rand(0,255);
        $b = rand(0,255);
        $c = rand(0,255);
        echo "<div style='background-color:rgb($a, $b, $c)'></div>";
    }
 
?>

<!-- écrire le code avant ce commentaire -->

</body>
</html>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<?php
$a = 15 /* rand(1,20); */
/*
 Afficher tout les nombres de 1 à 100, tout les multiples de $a doivent êtres colorés en rouge.
*/
?>
<!-- écrire le code après ce commentaire -->
<?php
    echo $a."<br>"."<br>";

    for ($i=1;$i<=100;$i++) {
        if ($i % $a == 0) {
            echo $i . "<br>";
        }
    }
    
    

?>


<!-- écrire le code avant ce commentaire -->

</body>
</html>


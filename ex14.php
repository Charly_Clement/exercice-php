<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>
<style>
    table {
        margin: 0;
    }
    td {
        border: 1px solid black;
        
    }
</style>
<?php
/*
 afficher les tables de multiplications de 0 à 10 dans un tableau html
*/
?>
<table>
<!-- écrire le code après ce commentaire -->
<?php
     for ($x = 0; $x <= 10; $x++) {
        echo "<tr>";
        for ($a = 0; $a <= 10; $a++) {
            echo"<td>";
            echo $x.' x '.$a.' = '.$x*$a;
            echo "</td>";
        }
        echo "</tr>";
     }
?>
</table>

<!-- écrire le code avant ce commentaire -->

</body>
</html>


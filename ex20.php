<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<?php
$i = rand(1,30);
/*
 indiquer si $a est un nombre premier (https://fr.wikipedia.org/wiki/Nombre_premier)
*/

?>

<!-- écrire le code après ce commentaire -->
<?php

function nombrePremier( $nombre ){
  
    //Pour i = 2, et i inférieur au nombre demandé, je regarde si mon nombre est divisible dans N.
   
    for ($i = 2; $i < $nombre; $i++){
      if ($nombre % $i == 0){
        $test = 1;
      }
    }
   
    if (isset($test)) { //Check si la variable existe.
      echo "$nombre n'est pas un nombre premier";
    }
    else{ //Elle n'existe pas, donc c'est premier.
      echo "$nombre est un nombre premier";
    }
        
  }
   
  nombrePremier(7901); //7901 est un nombre premier.
  echo "<br/>";
  nombrePremier(10); //10 n'est pas un nombre premier.

?>


<!-- écrire le code avant ce commentaire -->

</body>
</html>


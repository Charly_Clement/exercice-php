<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>
<a href="#">Voir l'article n°1</a>
<a href="#">Voir l'article n°2</a>
<a href="#">Voir l'article n°3</a>
<a href="#">Voir l'article n°4</a>
<a href="#">Voir l'article n°5</a>
<?php
/*
 Continuez la liste de liens ci-dessus jusqu'à l'article 100 grâce à PHP
*/
?>

<!-- écrire le code après ce commentaire -->
<?php
    $a = 6;
    for ($a=6; $a<=100;$a++) {
        echo '<a href="#">Voir' . "l'article n°" . $a.'</a>';
    }
?>


<!-- écrire le code avant ce commentaire -->

</body>
</html>


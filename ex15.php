<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<?php
/*
 afficher et prolonger sur 50 lignes le motif suivant grace à PHP:
    #
    ##
    ###
    ####
    #####
*/

?>

<!-- écrire le code après ce commentaire -->
<?php
    $i = 50;
    $h ="#";
    for ($i = 1; $i <= 50; $i++) {
        echo $h . "<br>";
        $h = $h . "#";
    }

?>


<!-- écrire le code avant ce commentaire -->

</body>
</html>

